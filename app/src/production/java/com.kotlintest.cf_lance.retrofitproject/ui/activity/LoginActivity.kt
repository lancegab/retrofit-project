package com.kotlintest.cf_lance.retrofitproject.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.kotlintest.cf_lance.retrofitproject.R
import com.kotlintest.cf_lance.retrofitproject.api.model.Authentication
import com.kotlintest.cf_lance.retrofitproject.api.model.LoginRequest
import com.kotlintest.cf_lance.retrofitproject.api.model.QuoteResponse
import com.kotlintest.cf_lance.retrofitproject.api.model.Session
import com.kotlintest.cf_lance.retrofitproject.api.service.FavQClient
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.NetworkBehavior
import java.util.concurrent.TimeUnit


class LoginActivity : AppCompatActivity() {

    private val okHttpClientBuilder = OkHttpClient.Builder()
    lateinit var serverClient: FavQClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        okHttpClientBuilder.addInterceptor(logging)

        val builder = Retrofit.Builder()
                .baseUrl("https://favqs.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())

        val retrofit = builder.build()

        val behavior = NetworkBehavior.create()
        behavior.setDelay(2000, TimeUnit.MILLISECONDS)
        behavior.setFailurePercent(20)
        behavior.setVariancePercent(50)

        serverClient = retrofit.create(FavQClient::class.java)

        getQuote()

        registerButton.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        loginButton.setOnClickListener {
            sendLoginRequest(
                    login.text.toString(),
                    password.text.toString()
            )
        }
    }

    private fun getQuote() {
        val call: Call<QuoteResponse> = serverClient.getQuote()

        call.enqueue(object : Callback<QuoteResponse> {
            override fun onResponse(call: Call<QuoteResponse>, response: Response<QuoteResponse>) {
                val q: QuoteResponse? = response.body()

                if (q != null) {
                    quote.text = q.quote.body
                }
            }

            override fun onFailure(call: Call<QuoteResponse>?, t: Throwable?) {
                Toast.makeText(this@LoginActivity, "error :(", Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun sendLoginRequest(login: String, password: String) {
        val call = serverClient.login(LoginRequest(Authentication(login, password)))

        call.enqueue(object : Callback<Session> {
            override fun onResponse(call: Call<Session>, response: Response<Session>) {
                if (response.isSuccessful) {
                    val session = response.body()
                    if (session != null) {
                        val intent = Intent(this@LoginActivity, NavigationActivity::class.java)

                        intent.putExtra("login", session.login)
                        intent.putExtra("userToken", session.userToken)

                        Toast.makeText(this@LoginActivity, "User logged in", Toast.LENGTH_SHORT).show()

                        startActivity(intent)
                    }
                } else {
                    Toast.makeText(this@LoginActivity, "There's something wrong", Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<Session>?, t: Throwable?) {
                Toast.makeText(this@LoginActivity, "Something went wrong!", Toast.LENGTH_SHORT).show()
            }
        })
    }
}
