package com.kotlintest.cf_lance.retrofitproject.api.service

import com.kotlintest.cf_lance.retrofitproject.api.model.*
import retrofit2.Call
import retrofit2.http.*

interface FavQClient {

    @GET("/api/qotd")
    fun getQuote(): Call<QuoteResponse>

    @Headers("Authorization: Token token=7e607156d57ead8b64e0358281e858a7")
    @POST("/api/users")
    fun createAccount(@Body user: SignUpRequest): Call<SignUpRequest>

    @Headers("Authorization: Token token=7e607156d57ead8b64e0358281e858a7")
    @POST("/api/session")
    fun login(@Body user: LoginRequest): Call<Session>

    @Headers("Authorization: Token token=7e607156d57ead8b64e0358281e858a7")
    @DELETE("api/session")
    fun logout(): Call<LogoutResponse>

    @Headers("Authorization: Token token=7e607156d57ead8b64e0358281e858a7")
    @GET("/api/users/{login}")
    fun getUser(@Path("login") login: String, @Header("User-Token") userToken: String): Call<UserProfile>

    @Headers("Authorization: Token token=7e607156d57ead8b64e0358281e858a7")
    @GET("/api/quotes")
    fun getQuotes(): Call<Quotes>

    @Headers("Authorization: Token token=7e607156d57ead8b64e0358281e858a7")
    @PUT("/api/users/{login}")
    fun updateUser(@Path("login") login: String, @Header("User-Token") userToken: String): Call<UserProfile>

}