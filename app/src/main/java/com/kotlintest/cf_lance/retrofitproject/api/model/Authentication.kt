package com.kotlintest.cf_lance.retrofitproject.api.model

data class Authentication(
        var login: String,
        var password: String
)