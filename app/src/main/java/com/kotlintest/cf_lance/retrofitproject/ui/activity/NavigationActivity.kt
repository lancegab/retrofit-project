package com.kotlintest.cf_lance.retrofitproject.ui.activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import com.kotlintest.cf_lance.retrofitproject.R
import com.kotlintest.cf_lance.retrofitproject.ui.fragment.ProfileFragment
import com.kotlintest.cf_lance.retrofitproject.ui.fragment.QuotesListFragment
import kotlinx.android.synthetic.main.activity_navigation.*

class NavigationActivity : AppCompatActivity() {
    private val fragmentManager = supportFragmentManager

    private val quotesListFragment = QuotesListFragment()
    private val profileFragment = ProfileFragment()

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                val fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragmentContainer, quotesListFragment).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                val bundle = Bundle()
                bundle.putString("login", intent.getStringExtra("login"))
                bundle.putString("userToken", intent.getStringExtra("userToken"))

                profileFragment.arguments = bundle

                val fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragmentContainer, profileFragment).commit()

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer, quotesListFragment).commit()


        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}
