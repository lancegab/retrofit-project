package com.kotlintest.cf_lance.retrofitproject.api.model

import com.google.gson.annotations.SerializedName

data class SignUpRequest(var user: User) {
    @SerializedName("User-Token")
    var userToken: String = ""
    var login: String = ""
}
