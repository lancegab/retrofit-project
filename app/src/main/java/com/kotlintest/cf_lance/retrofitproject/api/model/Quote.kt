package com.kotlintest.cf_lance.retrofitproject.api.model

data class Quote(
        var body: String,
        var author: String
)