package com.kotlintest.cf_lance.retrofitproject.api.model

data class QuoteResponse(
        var date: String,
        var quote: Quote
)