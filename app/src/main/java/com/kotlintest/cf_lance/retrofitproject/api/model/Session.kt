package com.kotlintest.cf_lance.retrofitproject.api.model

import com.google.gson.annotations.SerializedName


data class Session(
        @SerializedName("User-Token")
        var userToken: String,
        var login: String,
        var email: String
)