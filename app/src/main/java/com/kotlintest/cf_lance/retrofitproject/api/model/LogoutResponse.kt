package com.kotlintest.cf_lance.retrofitproject.api.model

data class LogoutResponse(var message: String)