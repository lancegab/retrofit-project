package com.kotlintest.cf_lance.retrofitproject.api.model

import retrofit2.http.Body

data class LoginRequest(
        @Body
        var user: Authentication
)