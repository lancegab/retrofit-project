package com.kotlintest.cf_lance.retrofitproject.api.model

data class Quotes(
        var quotes: MutableList<Quote>
)