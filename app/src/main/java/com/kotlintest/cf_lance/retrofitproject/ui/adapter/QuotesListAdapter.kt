package com.kotlintest.cf_lance.retrofitproject.ui.adapter


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.kotlintest.cf_lance.retrofitproject.R
import com.kotlintest.cf_lance.retrofitproject.api.model.Quote


class QuotesListAdapter(private val quotes: MutableList<Quote>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_quotes_list, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val quote = quotes[position]
        val h = holder as ViewHolder
        holder.bind(quote)
    }

    override fun getItemCount(): Int = quotes.size

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        @BindView(R.id.body)
        lateinit var bodyTextView: TextView
        @BindView(R.id.author)
        lateinit var authorTextView: TextView

        init {
            ButterKnife.bind(this, view)
        }

        fun bind(quote: Quote) = with(view) {
            bodyTextView.text = quote.body
            authorTextView.text = quote.author
        }
    }
}