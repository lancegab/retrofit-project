package com.kotlintest.cf_lance.retrofitproject.api.model

data class User(
        var login: String,
        var email: String,
        var password: String
)
