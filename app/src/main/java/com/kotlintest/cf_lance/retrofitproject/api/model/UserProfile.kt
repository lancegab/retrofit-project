package com.kotlintest.cf_lance.retrofitproject.api.model

data class UserProfile(
        var login: String,
        var pic_url: String,
        var public_favorites_count: Int,
        var followers: Int,
        var following: Int,
        var pro: Boolean
)