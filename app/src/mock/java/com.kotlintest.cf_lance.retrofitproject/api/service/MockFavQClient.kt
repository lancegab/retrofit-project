package com.kotlintest.cf_lance.retrofitproject.api.service

import com.kotlintest.cf_lance.retrofitproject.api.model.*
import retrofit2.Call
import retrofit2.mock.BehaviorDelegate

class MockFavQClient(val delegate: BehaviorDelegate<FavQClient>) : FavQClient {

    private val quote = QuoteResponse(
            "01-01-01",
            Quote("To be or not to be", "Pauline")
    )

    private val quotes = Quotes(
            mutableListOf(
                    Quote("Awesome!", "Ronald"),
                    Quote("Wow!", "Pauline"),
                    Quote("Cool!", "Lance"))
    )

    private val session = Session(
            "",
            "TestUser",
            "test.user@testing.com"
    )

    private val userProfile = UserProfile(
            "TestUser",
            "",
            315,
            824,
            805,
            true
    )

    override fun getQuote(): Call<QuoteResponse> = delegate.returningResponse(quote).getQuote()

    override fun createAccount(user: SignUpRequest): Call<SignUpRequest> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun login(user: LoginRequest): Call<Session> {
        val session = Session(
                "a1b2c3d4e5",
                "TestUser",
                "test.email@gmail.com"
        )

        return delegate.returningResponse(session).login(user)
    }

    override fun logout(): Call<LogoutResponse> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getUser(login: String, userToken: String): Call<UserProfile> = delegate.returningResponse(userProfile).getUser(userProfile.login, "abcdefg12345")

    override fun getQuotes(): Call<Quotes> = delegate.returningResponse(quotes).getQuotes()

    override fun updateUser(login: String, userToken: String): Call<UserProfile> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
