package com.kotlintest.cf_lance.retrofitproject.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.kotlintest.cf_lance.retrofitproject.R
import com.kotlintest.cf_lance.retrofitproject.api.model.SignUpRequest
import com.kotlintest.cf_lance.retrofitproject.api.model.User
import com.kotlintest.cf_lance.retrofitproject.api.service.FavQClient
import kotlinx.android.synthetic.main.activity_register.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RegisterActivity : AppCompatActivity() {

    private lateinit var serverClient: FavQClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val okHttpClientBuilder = OkHttpClient.Builder()

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        okHttpClientBuilder.addInterceptor(logging)

        val builder = Retrofit.Builder()
                .baseUrl("https://favqs.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())

        val retrofit = builder.build()

        serverClient = retrofit.create(FavQClient::class.java)

        registerButton.setOnClickListener{
            val user = User(
                    login.text.toString(),
                    email.text.toString(),
                    password.text.toString()
            )

            sendRegisterRequest(user)
        }
    }

    private fun sendRegisterRequest(user: User) {
        val call = serverClient.createAccount(SignUpRequest(user))

        call.enqueue(object: Callback<SignUpRequest> {
            override fun onResponse(call: Call<SignUpRequest>?, response: Response<SignUpRequest>?) {
                if(response!!.isSuccessful){
                    Toast.makeText(this@RegisterActivity, "User added", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this@RegisterActivity, "There's something wrong", Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<SignUpRequest>?, t: Throwable?) {
                Toast.makeText(this@RegisterActivity, "Something went wrong!", Toast.LENGTH_SHORT).show()
            }
        })
    }
}
