package com.kotlintest.cf_lance.retrofitproject.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.kotlintest.cf_lance.retrofitproject.R
import com.kotlintest.cf_lance.retrofitproject.api.model.LogoutResponse
import com.kotlintest.cf_lance.retrofitproject.api.model.UserProfile
import com.kotlintest.cf_lance.retrofitproject.api.service.FavQClient
import com.kotlintest.cf_lance.retrofitproject.api.service.MockFavQClient
import com.kotlintest.cf_lance.retrofitproject.ui.activity.LoginActivity
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import retrofit2.Call
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.MockRetrofit

class ProfileFragment : Fragment() {

    lateinit var apiCall: Call<UserProfile>
    private lateinit var mockClient: MockFavQClient
    private lateinit var serverClient: FavQClient

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val okHttpClientBuilder = OkHttpClient.Builder()

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        okHttpClientBuilder.addInterceptor(logging)

        val builder = Retrofit.Builder()
                .baseUrl("https://favqs.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())

        val retrofit = builder.build()

        val mockRetrofit = MockRetrofit.Builder(retrofit).build()
        val delegate = mockRetrofit.create(FavQClient::class.java)
        mockClient = MockFavQClient(delegate)
        serverClient = retrofit.create(FavQClient::class.java)


        val view = inflater!!.inflate(R.layout.fragment_profile, container, false)
        sendGetUserRequest()
        view.logout.setOnClickListener {
            sendLogoutRequest()
        }

        return view
    }

    private fun sendGetUserRequest() {
        apiCall = mockClient.getUser(arguments.getString("login"), arguments.getString("userToken"))

        apiCall.enqueue(object : Callback<UserProfile> {
            override fun onResponse(call: retrofit2.Call<UserProfile>?, response: Response<UserProfile>) {
                if (response.isSuccessful) {
                    val userProfile = response.body()

                    if (userProfile != null) {
                        login.text = userProfile.login
                        publicFavoritesCount.text = userProfile.public_favorites_count.toString()
                        followers.text = userProfile.followers.toString()
                        following.text = userProfile.following.toString()
                    }
                }
            }

            override fun onFailure(call: retrofit2.Call<UserProfile>?, t: Throwable?) {
                Log.i("FAILED", "FAILED")
            }
        })

    }

    private fun sendLogoutRequest() {
        val call = serverClient.logout()

        call.enqueue(object : Callback<LogoutResponse> {
            override fun onResponse(call: retrofit2.Call<LogoutResponse>?, response: Response<LogoutResponse>) {
                if (response.isSuccessful) {
                    val logout = response.body()
                    val intent = Intent(activity, LoginActivity::class.java)

                    if (logout != null) {
                        Toast.makeText(view!!.context, logout.message, Toast.LENGTH_SHORT).show()
                    }
                    startActivity(intent)
                }
            }

            override fun onFailure(call: retrofit2.Call<LogoutResponse>?, t: Throwable?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })

    }

    override fun onDetach() {
        super.onDetach()
        apiCall.cancel()
    }
}