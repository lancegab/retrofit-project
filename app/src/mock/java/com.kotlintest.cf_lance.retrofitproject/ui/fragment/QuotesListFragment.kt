package com.kotlintest.cf_lance.retrofitproject.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kotlintest.cf_lance.retrofitproject.R
import com.kotlintest.cf_lance.retrofitproject.api.model.Quotes
import com.kotlintest.cf_lance.retrofitproject.api.service.FavQClient
import com.kotlintest.cf_lance.retrofitproject.api.service.MockFavQClient
import com.kotlintest.cf_lance.retrofitproject.ui.adapter.QuotesListAdapter
import kotlinx.android.synthetic.main.fragment_quotes_list.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.MockRetrofit

class QuotesListFragment : Fragment() {

    private lateinit var adapter: QuotesListAdapter

    private lateinit var apiCall: Call<Quotes>

    private lateinit var mockClient: MockFavQClient
    private lateinit var serverClient: FavQClient

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_quotes_list, container, false)

        val okHttpClientBuilder = OkHttpClient.Builder()

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        okHttpClientBuilder.addInterceptor(logging)

        val builder = Retrofit.Builder()
                .baseUrl("https://favqs.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())

        val retrofit = builder.build()

        val mockRetrofit = MockRetrofit.Builder(retrofit).build()
        val delegate = mockRetrofit.create(FavQClient::class.java)
        mockClient = MockFavQClient(delegate)
        serverClient = retrofit.create(FavQClient::class.java)

        if (view is RecyclerView) {
            val context = view.getContext()
            view.layoutManager = LinearLayoutManager(context)
            getQuotes()
        }

        return view
    }

    private fun getQuotes() {
        apiCall = mockClient.getQuotes()


        apiCall.enqueue(object : Callback<Quotes> {
            override fun onResponse(call: retrofit2.Call<Quotes>, response: Response<Quotes>) {
                if (response.isSuccessful) {
                    val quotes = response.body()

                    if (quotes != null) {
                        adapter = QuotesListAdapter(quotes.quotes)
                        quotesRecyclerView.adapter = adapter
                    }
                }
            }


            override fun onFailure(call: retrofit2.Call<Quotes>, t: Throwable?) {
                Log.i("FAILED", "FAILED")
            }
        })
    }

    override fun onDetach() {
        super.onDetach()
        apiCall.cancel()
    }

}
